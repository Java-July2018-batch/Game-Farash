package org.javaclass.farash;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Deck {

    List<Card> cards = new ArrayList<Card>();

    public Deck() {
        initializeDeck();
    }

    public List getDeck(){
        return cards;
    }

    public void initializeDeck(){
        for(int i =0; i < 4; i++){
            for(int j=2; j < 15; j++){
                Card card = new Card(j,i);
                cards.add(card);
                //System.out.println(card);
            }
        }
    }
    public Card giveCard(){
        Card card = cards.remove(cards.size()-1);
        return card;
    }

    public void shuffle(){
        Collections.shuffle((List) cards);
    }










}
