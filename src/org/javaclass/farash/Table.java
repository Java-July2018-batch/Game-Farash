package org.javaclass.farash;

import java.util.ArrayList;

public class Table {


    ArrayList<Player> players = new ArrayList<Player>();
    Deck deck = new Deck();

    public void addDeck(Deck deck){
        this.deck = deck;
    }
    public Deck getDeck(){
        return deck;
    }

    public boolean addPlayer(Player player){
        if(players.size() == 17)
            return false;
        players.add(player);
            return true;
    }


    public ArrayList<Player> getPlayers(){

        return players;
    }

    public void deal(){
        for(int j=0; j < 3; j++) {
            for (int i = 0; i < players.size(); i++) {
                Card card = deck.giveCard();
                players.get(i).getHand().addCard(card);
            }
        }


    }
}
