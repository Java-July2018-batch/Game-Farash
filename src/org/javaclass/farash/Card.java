package org.javaclass.farash;

public class Card {
    final int NUMBER;
    final int TYPE;

    public Card(final int x, final int y) {
        NUMBER = x;
        TYPE = y;
    }

    public final int getNumber(){
        return NUMBER;
    }
    public final int getType(){
        return TYPE;
    }

    public String toString(){

        char charAKQJ = ' ';
        switch (NUMBER){


            case 11:
                charAKQJ = Constant.JACK_SIGN;
                break;
            case 12:
                charAKQJ = Constant.QUEEN_SIGN;
                break;
            case 13:
                charAKQJ = Constant.KING_SIGN;
                break;
            case 14:
                charAKQJ = Constant.ACE_SIGN;
                break;

        }

        char sign = ' ';
        switch (TYPE) {
            case 0:
                sign = Constant.SPADE_SIGN;
                break;
            case 1:
                sign = Constant.CLUB_SIGN;
                break;
            case 2:
                sign = Constant.HEART_SIGN;
                break;
            case 3:
                sign = Constant.DIAMOND_SIGN;
                break;
        }

        String Face_Value = "" + NUMBER;

        if (NUMBER >= 11)
        {
            Face_Value = "" + charAKQJ;
        }
        return "" + Face_Value + sign ;
    }
}
