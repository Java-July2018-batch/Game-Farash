package org.javaclass.farash;

import java.util.ArrayList;

public class Hand {


    ArrayList<Card> cards = new ArrayList<Card>();


    public void addCard(Card card){
        cards.add(card);
    }

    public boolean isPair(){
        if(is3ofaKind())
            return false;
        if((cards.get(0).getNumber() == cards.get(1).getNumber()) || (cards.get(0).getNumber() == cards.get(2).getNumber()) || (cards.get(1).getNumber() == cards.get(2).getNumber()))
            return true;
        else
            return false;
    }

    public void orderCards(){
        ArrayList<Card> newList = new ArrayList<Card>();
        if(isPair()){
            Card card1 = cards.remove(0);
            Card card2 = cards.remove(0);
            Card card3 = cards.remove(0);

            if(card1.NUMBER == card2.NUMBER){
                newList.add(card1);
                newList.add(card2);
                newList.add(card3);
            }
            if(card1.NUMBER == card3.NUMBER){
                newList.add(card1);
                newList.add(card3);
                newList.add(card2);
            }
            if(card2.NUMBER == card3.NUMBER){
                newList.add(card2);
                newList.add(card3);
                newList.add(card1);
            }

        }



        while(!cards.isEmpty()){
            Card card = cards.remove(0);

            if(newList.isEmpty()) {
                newList.add(card);
            }
            else {
                boolean isAdded = false;
                for(int i=0; i<newList.size();i++) {
                    if (card.NUMBER >= newList.get(i).NUMBER) {
                        newList.add(i, card);
                        isAdded = true;
                        break;
                    }
                }
                if(isAdded == false){
                    newList.add(card);
                }
            }
        }
        this.cards = newList;

    }
    public boolean isRun(){
        orderCards();
                if((cards.get(0).NUMBER == Constant.ACE) && (cards.get(1).NUMBER == 3) && (cards.get(2).NUMBER == 2)){
                    return true;
                }
                if((cards.get(0).NUMBER == (cards.get(1).NUMBER +1) && (cards.get(0).NUMBER == (cards.get(2).NUMBER+2)))){
                    return true;
        }
        return false;
    }

    public boolean isFarash()
    {
        if((cards.get(0).TYPE == cards.get(1).TYPE) && (cards.get(1).TYPE == cards.get(2).TYPE)) {

            return true;
        }

        return false;
    }

    public boolean is3ofaKind(){
        if((cards.get(0).NUMBER==cards.get(1).NUMBER)   &&  (cards.get(1).NUMBER == cards.get(2).NUMBER)){
            return true;
        }

        return false;
    }


    public double getNumericValue(){

        String str = "";
        int aa = 1; //is a top unless ...
        if(is3ofaKind()){
            aa= 6;
        }
        else if(isRun() && isFarash()){
            aa = 5;
        }
        else if (isRun()) {
            aa = 4;
        }
        else if (isFarash()){
            aa = 3;
        }
        else if(isPair()){
            aa = 2;
        }

        str = str + aa + ".";

        orderCards();

        Card card1 = cards.get(0);
        if(card1.NUMBER < 10)
            str = str + 0;
        str = str + card1.NUMBER;

        Card card2 = cards.get(1);
        if(card2.NUMBER < 10)
            str = str + 0;
        str = str + card2.NUMBER;

        Card card3 = cards.get(2);
        if(card3.NUMBER < 10)
            str = str + 0;
        str = str + card3.NUMBER;

        return Double.parseDouble(str);
    }




    public String toString(){
        orderCards();
        String returnString = "";
        for (int i =0; i < cards.size() ; i++){
            returnString += " " + cards.get(i);
        }
        return returnString;
    }
}
