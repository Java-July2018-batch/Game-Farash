package org.javaclass.farash;

import java.util.*;

import static java.util.Collection.*;
import static java.util.Collections.shuffle;

public class MainClass {

    public static void main(String[] args) {

        Table table = new Table();

        Player player1 = new Player("Player 1");
        Player player2 = new Player("Player 2");

        table.addPlayer(player1);
        table.addPlayer(player2);

        Deck deck = new Deck();
        table.addDeck(deck);
        table.getDeck().shuffle();

        table.deal();
        Player winner = null;
        for(int i=0; i<table.getPlayers().size(); i++){
            if(winner == null)
                winner = table.getPlayers().get(0);


            System.out.println(table.getPlayers().get(i).getPlayerName() + " hand is \t" + table.getPlayers().get(i).getHand());
            System.out.println("Numeric Value of this hand is " +table.getPlayers().get(i).getHand().getNumericValue());


            if(winner.getHand().getNumericValue() < table.getPlayers().get(i).getHand().getNumericValue())
                winner = table.getPlayers().get(i);
        }


        System.out.println("\nThe winner is " +"\"" +winner.getPlayerName() + "\"");
    }



    private static void Test1(){

        System.out.println("spade" + Constant.SPADE_SIGN);
        System.out.println("club" + Constant.CLUB_SIGN);
        System.out.println("heart" + Constant.HEART_SIGN);
        System.out.println("diamond" + Constant.DIAMOND_SIGN);

    }


    // we want to print number and sign of each card in deck which should be 52
    private static void Test2(ArrayList<Card> cards){

        for(int i =0; i<52; i++){
            System.out.print(i+"\t"+cards.get(i).NUMBER);




            switch (cards.get(i).TYPE) {
                case 0:
                    System.out.println(Constant.SPADE_SIGN);
                    break;
                case 1:
                    System.out.println(Constant.CLUB_SIGN);
                    break;
                case 2:
                    System.out.println(Constant.HEART_SIGN);
                    break;
                case 3:
                    System.out.println(Constant.DIAMOND_SIGN);
                    break;
            }
        }

    }
}
