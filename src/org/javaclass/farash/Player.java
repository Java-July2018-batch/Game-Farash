package org.javaclass.farash;

import java.util.ArrayList;

public class Player {
    String Name;
    Hand hand = new Hand();

    public Player(String name) {
        Name = name;
    }

    public String getPlayerName(){

        return Name;
    }

    public Hand getHand(){
        return hand;
    }
}
