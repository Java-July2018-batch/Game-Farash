package org.javaclass.farash;

public final class Constant {

    // DEFINE

    public static final int JACK = 11;
    public static final int QUEEN = 12;
    public static final int KING = 13;
    public static final int ACE = 14;

    public static final int SPADE = 0;
    public static final int CLUB = 1;
    public static final int HEART = 2;
    public static final int DIAMOND = 3;


    public static final char SPADE_SIGN = '\u2660';
    public static final char CLUB_SIGN = '\u2663';
    public static final char HEART_SIGN = '\u2764';
    public static final char DIAMOND_SIGN = '\u2666';


    public static final char ACE_SIGN = 'A';
    public static final char JACK_SIGN = 'J';
    public static final char QUEEN_SIGN = 'Q';
    public static final char KING_SIGN = 'K';






}
